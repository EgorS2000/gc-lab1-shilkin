let rgb = new RGB(0, 0, 0);
let xyz = new XYZ(0, 0, 0);
let cmyk = new CMYK(0, 0, 0, 0);

function palette(event) {
    rgb = hexToRgb(event.target.value);
    calculate(rgb);
}

function sliderMoveRGB(s) {
    solveAll(rgb, s);
}

function sliderMoveXYZ(s) {
    solveAll(xyz, s);
}

function sliderMoveCMYK(s) {
    solveAll(cmyk, s);
}

function setLoggerValue(message) {
    $('#logger').val(message);
}

function getVals(s) {
    return parseInt($('#' + s).val());
}

function solveAll(color, id) {
    color[id.toLowerCase()] = getVals(id);
    calculate(color);
}

function calculate(color){
    rgb = ColorConverter.toRGB(color);
    xyz = ColorConverter.toXYZ(color);
    cmyk = ColorConverter.toCMYK(color);
    correctSliders();
}

function correctSliders() {
    [rgb, xyz, cmyk].forEach(function (scheme) {
        Object.keys(scheme).forEach(function (elem) {
        	$('#' + elem.toUpperCase()).val(scheme[elem]);
        })
    });
    console.log("rgb: " + rgb.r + "|" + rgb.g + "|" + rgb.b);
    console.log("xyz: " + xyz.x + "|" + xyz.y + "|" + xyz.z);
    console.log("cmyk: " + cmyk.cc + "|" + cmyk.mm + "|" + cmyk.yy+ "|" + cmyk.kk);
    $('#colorWell').val(rgbToHex(rgb));
}

function RGB(r, g, b) {
    if (r < 0) {
        r = 0;
        setLoggerValue('R was less than 0 so have been rounded.');
    }
    if (g < 0) {
        g = 0;
        setLoggerValue('G was less than 0 so have been rounded.');
    }
    if (b < 0) {
        b = 0;
        setLoggerValue('B was less than 0 so have been rounded.');
    }

    if (r > 255) {
        r = 255;
        setLoggerValue('R was more than 255 so have been rounded.');
    }
    if (g > 255) {
        g = 255;
        setLoggerValue('G was more than 255 so have been rounded.');
    }
    if (b > 255) {
        b = 255;
        setLoggerValue('B was more than 255 so have been rounded.');
    }

    this.r = Math.round(r);
    this.g = Math.round(g);
    this.b = Math.round(b);
}

function XYZ(x, y, z) {
    if (x < 0) {
        x = 0;
        setLoggerValue('X was less than 0 so have been rounded.');
    }
    if (y < 0) {
        y = 0;
        setLoggerValue('Y was less than 0 so have been rounded.');
    }
    if (z < 0) {
        z = 0;
        setLoggerValue('Z was less than 0 so have been rounded.');
    }

    if (x > 100) {
        x = 100;
        setLoggerValue('X was more than 100 so have been rounded.');
    }
    if (y > 100) {
        y = 100;
        setLoggerValue('Y was more than 100 so have been rounded.')
    }
    if (z > 100) {
        z = 100;
        setLoggerValue('Z was more than 100 so have been rounded.')
    }

    this.x = Math.round(x);
    this.y = Math.round(y);
    this.z = Math.round(z);
}

function CMYK(c, m, y, k) {
    if (c < 0) {
        c = 0;
        setLoggerValue('C was less than 0 so have been rounded.');
    }
    if (m < 0) {
        m = 100;
        setLoggerValue('M was less than 0 so have been rounded.');
    }
    if (y < 0) {
        y = 0;
        setLoggerValue('Y was less than 0 so have been rounded.');
    }
    if (k < 0) {
        k = 0;
        setLoggerValue('K was less than 0 so have been rounded.');
    }

    if (c > 100) {
        c = 100;
        setLoggerValue('C was more than 100 so have been rounded.')
    }
    if (m > 100) {
        m = 100;
        setLoggerValue('M was more than 100 so have been rounded.')
    }
    if (y > 100) {
        y = 100;
        setLoggerValue('Y was more than 100 so have been rounded.')
    }
    if (k > 100) {
        k = 100;
        setLoggerValue('K was more than 100 so have been rounded.')
    }

    this.cc = Math.round(c);
    this.mm = Math.round(m);
    this.yy = Math.round(y);
    this.kk = Math.round(k);
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? new RGB(
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16))
        : null;
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length === 1 ? "0" + hex : hex;
}

function rgbToHex(rgb) {
    return "#" + componentToHex(rgb.r) + componentToHex(rgb.g) + componentToHex(rgb.b);
}

var ColorConverter = {
    _RGBtoXYZ: function (RGB) {
        function F1(x) {
            if (x < 0.04045) {
                return x / 12.92;
            }
            else {
                return Math.pow((x + 0.055) / 1.055, 2.4);
            }
        }

        var Rn = F1(RGB.r/255)*100;
        var Gn = F1(RGB.g/255)*100;
        var Bn = F1(RGB.b/255)*100;

        var x = 0.412453*Rn + 0.357580*Gn + 0.180423*Bn;
        var y = 0.212671*Rn + 0.715160*Gn + 0.072169*Bn;
        var z = 0.019334*Rn + 0.119193*Gn + 0.950227*Bn;

        return new XYZ(x, y, z);
    },
    _XYZtoRGB: function (XYZ) {

        function F1(x) {
            if(x >= 0.0031308) {
                return 1.055*Math.pow(x, 1/2.4) - 0.055;
            }
            else {
                return 12.92*x;
            }
        }

        var x = XYZ.x/100;
        var y = XYZ.y/100;
        var z = XYZ.z/100;

        var Rn = 3.2406*x - 1.5372*y - 0.4986*z;
        var Gn = -0.9689*x + 1.8758*y + 0.0415*z;
        var Bn = 0.0557*x - 0.2040*y + 1.0570*z;

        var r = F1(Rn)*255;
        var g = F1(Gn)*255;
        var b = F1(Bn)*255;

        return new RGB(r, g, b);
    },
    _CMYKtoRGB: function(CMYK) {
        let Cn = CMYK.cc/100;
        let Mn = CMYK.mm/100;
        let Yn = CMYK.yy/100;
        let Kn = CMYK.kk/100;

       let Rn = 1 - Math.min( 1, Cn * ( 1 - Kn ) + Kn );
       let Gn = 1 - Math.min( 1, Mn * ( 1 - Kn ) + Kn );
       let Bn = 1 - Math.min( 1, Yn * ( 1 - Kn ) + Kn );

        let r = Math.round( Rn * 255 );
        let g = Math.round( Gn * 255 );
        let b = Math.round( Bn * 255 );
        return new RGB(r, g, b);
    },
    _RGBtoCMYK: function(RGB) {
        let Rn = RGB.r / 255;
        let Gn = RGB.g / 255;
        let Bn = RGB.b / 255;

        let Kn = Math.min( 1 - Rn, 1 - Gn, 1 - Bn );
        let Cn = ( 1 - Rn - Kn ) / ( 1 - Kn );
        let Mn = ( 1 - Gn - Kn ) / ( 1 - Kn );
        let Yn = ( 1 - Bn - Kn ) / ( 1 - Kn );

        let c = Math.round( Cn * 100 );
        let m = Math.round( Mn * 100 );
        let y = Math.round( Yn * 100 );
        let k = Math.round( Kn * 100 );
        return new CMYK(c, m, y, k);
    },

    toRGB: function (o) {
        if (o instanceof RGB) {
            return o;
        }
        if (o instanceof XYZ) {
            return this._XYZtoRGB(o);
        }
        if (o instanceof CMYK) {
            return this._CMYKtoRGB(o);
        }
    },
    toXYZ: function (o) {
        if (o instanceof XYZ) {
            return o;
        }
        if (o instanceof RGB) {
            return this._RGBtoXYZ(o);
        }

        if (o instanceof CMYK) {
            return this._RGBtoXYZ(this._CMYKtoRGB(o));
        }
    },
    toCMYK: function (o) {
        if (o instanceof CMYK) {
            return o;
        }
        if (o instanceof RGB) {
            return this._RGBtoCMYK(o)
        }
        if (o instanceof XYZ) {
            return this._RGBtoCMYK(this._XYZtoRGB(o));
        }

    }
}

var showingTooltip;
var tooltipElem;
var field;

document.onmousedown = function(e) {
      var target = e.target;
      var tooltip = target.value;
      field = document.getElementById(target.id + "F");
      if (!tooltip) return;

      tooltipElem = document.createElement('div');
      tooltipElem.className = 'tooltip';
      tooltipElem.innerHTML = tooltip;
      document.body.appendChild(tooltipElem);

      var coords = target.getBoundingClientRect();

      var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
      if (left < 0) left = 0;

      var top = coords.top - tooltipElem.offsetHeight - 5;
      if (top < 0) {
        top = coords.top + target.offsetHeight + 5;
      }

      tooltipElem.style.left = left + 'px';
      tooltipElem.style.top = top + 'px';

      showingTooltip = tooltipElem;
};

document.onmouseup = function(e) {

	if (showingTooltip) {
	    document.body.removeChild(showingTooltip);
	    showingTooltip = null;
	}
}

document.onmousemove = function(e) {
	if (showingTooltip) {
	    try{
            tooltipElem.innerHTML = e.target.value;
            field.value = e.target.value;
        } catch (e) {
            console.log("Error! Select a color from the palette")
        }

	}
}
